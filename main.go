package main

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"github.com/streadway/amqp"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Status string

type Severity int

const (
	ONGOING  Status = "ONGOING"
	RESOLVED Status = "RESOLVED"

	OK       Severity = 0
	Minor    Severity = 1
	Major    Severity = 2
	Critical Severity = 3
)

type Event struct {
	Source    string   `json:"source"`
	Component string   `json:"component"`
	Resource  string   `json:"resource"`
	Severity  Severity `json:"crit"`
	Message   string   `json:"message"`
	Ts        int      `json:"timestamp"`
}

type Alarm struct {
	Component string   `bson:"component"`
	Resource  string   `bson:"resource"`
	Severity  Severity `bson:"crit"`
	LastMsg   string   `bson:"last_msg"`
	FirstMsg  string   `bson:"first_msg"`
	StartTs   int      `bson:"start_time"`
	LastTs    int      `bson:"last_time"`
	Status    Status   `bson:"status"`
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func initMongo() *mongo.Client {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	failOnError(err, "mongo.Connect has failed")

	err = client.Ping(context.TODO(), nil)
	failOnError(err, "Failed to connect to MongoDB server")
	return client
}

func main() {
	conn, err := amqp.Dial("amqp://test:test@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare("Alarm", false, false, false, false, nil)
	failOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	failOnError(err, "Failed to register a consumer")

	mongoCl := initMongo()
	collection := mongoCl.Database("test").Collection("test_technique")

	go func() {
		for d := range msgs {
			var e Event
			json.Unmarshal(d.Body, &e)
			log.Printf("Event received: %s on %s, severity: %d", e.Message, e.Component, e.Severity)
			// TODO implement full insert/update logic based on Severity & Component/Resource couple
			a := Alarm{
				Component: e.Component,
				Resource:  e.Resource,
				Severity:  e.Severity,
				FirstMsg:  e.Message,
				StartTs:   e.Ts,
				Status:    ONGOING,
			}
			ctx, _ := context.WithTimeout(context.Background(), 3*time.Second)
			_, err := collection.InsertOne(ctx, a)
			failOnError(err, "Failed to insert Alarm record")
		}
	}()

	log.Printf("Waiting for events. To exit press CTRL+C")
	forever := make(chan bool)
	<-forever
}
