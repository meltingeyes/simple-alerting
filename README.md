### Simple alerts monitoring engine
The engine is a simple Go daemon. It listens for alerting events on RabbitMQ queue _Alerts_
and writes alarm objects to MongoDB collection.

### Building and running
See included *Makefile*.
